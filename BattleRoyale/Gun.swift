import Foundation

class Gun : IWeapon {
    var name: String
    var baseDamage: Double
    var baseAccuracy: Int
    var hitsNumber: Int
    var criticalRate: Int
    var bullets: Int
    var description: String {
        return self.name
    }
    
    init(name: String, baseDamage: Double, baseAccuracy: Int, hitsNumber: Int, criticalRate: Int, bullets: Int) {
        self.name = name
        self.baseDamage = baseDamage
        self.baseAccuracy = baseAccuracy
        self.hitsNumber = hitsNumber
        self.criticalRate = criticalRate
        self.bullets = bullets
    }
    
    func damage() -> Double {
        return (self.baseDamage + self.bonus()) / (self.bullets > 0 ? 1 : 5)
    }
    
    func bonus() -> Double {
        return Double(arc4random_uniform(UInt32(self.baseDamage * 0.5)))
    }
}
