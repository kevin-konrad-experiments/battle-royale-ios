import Foundation

let protectionRate = 15
let pauseBetweenRounds = 5
var round = 0;
var characters = [
    Character(name: "Kevin"),
    Character(name: "Elena"),
    Character(name: "Thomas"),
    Character(name: "Maxime"),
    Character(name: "Serey")
]

repeat {
    sleep(UInt32(pauseBetweenRounds))
    round += 1
    print("===== ROUND " + String(round) + " =====")
    
    for character in characters {
        if (character.health == 0) {
            print(String(describing: character))
            continue
        }
        
        let rand = Int(arc4random_uniform(UInt32(100 + 1)))
        
        if rand <= 100 - protectionRate {
            var randomChar = 0
            var target = characters[randomChar]
            
            repeat {
                randomChar = Int(arc4random_uniform(UInt32(characters.count)))
                target = characters[randomChar]
            } while (target.name == character.name || target.health == 0)
            
            var hits = 0
            while hits < character.weapon.hitsNumber {
                character.attack(target: target)
                hits += 1
            }
        } else {
            character.protect()
        }
    }
    
    print("\n")
} while (characters.filter({ $0.health > 0 }).count > 1)

print("And the winner iiiis : ")
characters.filter({ $0.health > 0 }).forEach({ print($0) })

print("===== STATS =====")
characters.forEach({ print($0.name + " | " + $0.weapon.name + " | Done: " + String($0.damagesDone) + " | Taken: " + String($0.damagesTaken)) })
