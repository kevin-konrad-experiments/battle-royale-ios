protocol IWeapon : CustomStringConvertible {
    var name: String { get set }
    var baseDamage: Double { get set }
    var baseAccuracy: Int { get set }
    var hitsNumber: Int{ get set }
    var criticalRate: Int { get set }
    
    func damage() -> Double
    func bonus() -> Double
}
