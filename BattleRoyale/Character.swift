import Foundation

var weapons: [IWeapon] = [
    Gun(name: "Kalash", baseDamage: 59, baseAccuracy: 85, hitsNumber: 1, criticalRate: 15, bullets: 25),
    Gun(name: "Bow", baseDamage: 78, baseAccuracy: 67, hitsNumber: 1, criticalRate: 60, bullets: 15),
    Gun(name: "Sniper", baseDamage: 306, baseAccuracy: 58, hitsNumber: 1, criticalRate: 64, bullets: 5),
    Sword(name: "Sword", baseDamage: 51, baseAccuracy: 100, hitsNumber: 1, criticalRate: 5),
    Sword(name: "Dagger", baseDamage: 36, baseAccuracy: 70, hitsNumber: 2, criticalRate: 60)
]

class Character : CustomStringConvertible {
    let baseHealth = 750
    let baseHitChance = 100
    let baseAtkMultiplier = 1.0
    let criticalAtkMultiplier = 1.3
    let baseProtectionDivider = 0.4
    let minKillHealthRegen = 50
    let maxKillHleathRegen = 75
    let firstBloodKillCount = 1
    let rampageKillCount = 2
    let monsterKillCount = 3
    
    var name: String
    var health: Int
    var protected: Bool
    var weapon: IWeapon
    var kills: Int
    var damagesDone: Double
    var damagesTaken: Double
    var description: String {
        return self.health == 0 ? self.name + " (DEAD)" : (self.protected ? "[P] " : "") + self.name + " (" + String(self.health) + ")"
    }
    
    init(name: String) {
        let randomWeapon = Int(arc4random_uniform(UInt32(weapons.count)))
        
        self.name = name
        self.health = baseHealth
        self.protected = false
        self.weapon = weapons.remove(at: randomWeapon)
        self.kills = 0
        self.damagesDone = 0
        self.damagesTaken = 0
    }
    
    func protect() {
        self.protected = true
        print(String(describing: self) + " protects himself reducing the next incoming attack by " + String(Int(baseProtectionDivider * 100)) + "%")
    }
    
    func attack(target: Character) {
        let randAtkAccuracy = Int(arc4random_uniform(UInt32(100 + 1)))
        let previousHealth = target.health
        var dmg = self.weapon.damage()
        var killMsg = ""
        var criticalMsg = ""
        var randHealthBonus = 0
        
        if let gun = self.weapon as? Gun {
            gun.bullets = max(gun.bullets - 1, 0)
        }

        if (randAtkAccuracy < self.weapon.baseAccuracy) {
            let randAtkCritical = Int(arc4random_uniform(UInt32(100 + 1)))
            
            if (randAtkCritical < self.weapon.criticalRate) {
                dmg *= criticalAtkMultiplier
                criticalMsg = " CRITICAL "
            }
            
            dmg *= (target.protected ? 1.0 - baseProtectionDivider : 1)
            target.protected = false
            target.health = max(target.health - Int(dmg), 0)
            target.damagesTaken += dmg
            self.damagesDone += dmg
        } else {
            print(String(describing: self) + " attacked " + String(describing: target) + " with " + String(describing: self.weapon) + " and missed")
            return
        }
        
        if (target.health == 0) {
            randHealthBonus = minKillHealthRegen + Int(arc4random_uniform(UInt32(maxKillHleathRegen - minKillHealthRegen + 1)))
            self.kills += 1
            killMsg = " (+ " + String(randHealthBonus) + " health bonus)"
            
            switch (self.kills) {
                case firstBloodKillCount:
                    killMsg += " FIRST BLOOD"
                    break
                case rampageKillCount:
                    killMsg += " RAMPAGE"
                    break
                case monsterKillCount:
                    killMsg += " MONSTER KILL"
                    break
                default:
                    break
            }
        }
        
        print(String(describing: self) + " attacked "  + target.name + " (" + String(previousHealth) + ") for " + String(Int(dmg)) + " damages with " + String(describing: self.weapon) + killMsg + criticalMsg)
        
        self.health += randHealthBonus
    }
}
