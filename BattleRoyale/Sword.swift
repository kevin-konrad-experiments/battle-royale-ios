import Foundation

class Sword : IWeapon {
    var name: String
    var baseDamage: Double
    var baseAccuracy: Int
    var hitsNumber: Int
    var criticalRate: Int
    
    var description: String {
        return self.name
    }
    
    init(name: String, baseDamage: Double, baseAccuracy: Int, hitsNumber: Int, criticalRate: Int) {
        self.name = name
        self.baseDamage = baseDamage
        self.baseAccuracy = baseAccuracy
        self.hitsNumber = hitsNumber
        self.criticalRate = criticalRate
    }
    
    func damage() -> Double {
        return self.baseDamage + self.bonus()
    }
    
    func bonus() -> Double {
        return Double(arc4random_uniform(UInt32(self.baseDamage * 0.5)))
    }
}
